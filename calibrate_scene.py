#!/usr/bin/env python3

#  ###########################################################################
 #  Copyright(C) 2020 Michal Charvat
 #
 #  This program is free software: you can redistribute it and/or modify
 #  it under the terms of the GNU Lesser General Public License as published by
 #  the Free Software Foundation, either version 3 of the License, or
 #  (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU Lesser General Public License for more details.
 #
 #  You should have received a copy of the GNU Lesser General Public License
 #  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#  ###########################################################################

import argparse
from matplotlib import pyplot as plt

from ThermoDetection.Scene import Scene
from ThermoDetection.SceneCalibrator import CalibratorStatic, CalibratorDynamic

if __name__ == '__main__':
    try:
        # parse arguments
        parser = argparse.ArgumentParser(description='Script to create, calibrate and save a scene object from a JSON config file.')
        parser.add_argument('-s', '--scene', dest='scene', required=True, help='Path to a scene .JSON config file.')
        parser.add_argument('-c', '--camera', dest='camera', help='Camara name to be calibrated.')
        parser.add_argument('-f', '--file', dest='calibration_file', help='Calibration .TIFF file to be used for camera calibrating.')
        args = parser.parse_args()

        # load a scene from config json
        scene = Scene.from_json_config(args.scene)
        print('Loaded:\n{}\nOpening camera view...'.format(scene))

        # calibrate a camera
        if args.camera:
            assert args.camera in scene.cameras, 'Camera not found in the scene: {}'.format(args.camera)

            if args.calibration_file:
                # calibrate from a .tiff file
                with CalibratorStatic(scene, args.camera, args.calibration_file) as calibrator:
                    calibrator.calibrate()

            elif scene.cameras[args.camera].ip:
                # calibrate from live stream
                with CalibratorDynamic(scene, args.camera, scene.cameras[args.camera].ip, scene.cameras[args.camera].port) as calibrator:
                    calibrator.calibrate()

            else:
                print('Calibration of the camera requires calibration .tiff file or ip,[port] specified in the config for the camera: {}'.format(args.camera))

            print('Camera position: {}  Calculated camera position: {}'.format(scene.cameras[args.camera].camera_position, scene.cameras[args.camera].world_camera_position))
            print('After calibration:\n{}'.format(scene))

        # display only
        plt.figure(args.scene)
        scene.draw_scene()
        plt.show()

        # confirm saving
        input('Press any key to confirm OVERWRITING the existing scene file: {}'.format(args.scene))

        # save a scene
        scene.save_json_config(args.scene)
        print('Scene saved: {}'.format(args.scene))

    except KeyboardInterrupt:
        pass