#  ###########################################################################
 #  Copyright(C) 2020 Michal Charvat
 #
 #  This program is free software: you can redistribute it and/or modify
 #  it under the terms of the GNU Lesser General Public License as published by
 #  the Free Software Foundation, either version 3 of the License, or
 #  (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU Lesser General Public License for more details.
 #
 #  You should have received a copy of the GNU Lesser General Public License
 #  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#  ###########################################################################

from typing import List, Tuple

class Boundary(object):
    def __init__(self, name: str, vertices: List[Tuple[int, int]], color: str = 'blue'):
        self.name: str = name
        self.color: str = color
        self.vertices: List[Tuple[int, int]] = vertices

        self.xmin = min(x for x, y in self.vertices)
        self.xmax = max(x for x, y in self.vertices)
        self.ymin = min(y for x, y in self.vertices)
        self.ymax = max(y for x, y in self.vertices)

    def __str__(self) -> str:
        string = 'BOUND: {}, {}, vertices: {}'.format(self.name, self.color, len(self.vertices))
        return string

    def serialize(self) -> dict:
        boundary = {
            'name': self.name,
            'color': self.color,
            'vertices': self.vertices
        }
        return boundary