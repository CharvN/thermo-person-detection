#  ###########################################################################
 #  Copyright(C) 2020 Michal Charvat
 #
 #  Thermo helper class/library allowing for image linear normalization,
 #  converting temperature to pixel values and vice versa (mapping for a
 #  specific camera), getting binary temp mask and finding border boxes around
 #  compact white objects.

 #  This program is free software: you can redistribute it and/or modify
 #  it under the terms of the GNU Lesser General Public License as published by
 #  the Free Software Foundation, either version 3 of the License, or
 #  (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU Lesser General Public License for more details.
 #
 #  You should have received a copy of the GNU Lesser General Public License
 #  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#  ###########################################################################

import cv2
import numpy as np
from typing import List, Tuple

from ThermoDetection import ThermoHelper

class ThermoDetector(object):
    def __init__(self, radiometry=True, radiometry_scale=100.0, ambient_temp=22.0, verbose=False, **kwargs):
        self.verbose = verbose

        # camera params
        self.radiometry = radiometry
        self.ambient_temp = ambient_temp
        self.radiometry_scale = radiometry_scale
        self.pixel_converter = ThermoHelper.TrueRadiometryPixelTempConvertor(radiometry_scale) if radiometry else ThermoHelper.FalseRadiometryPixelTempConvertor(ambient_temp)

        # detection hyperparameters
        self.adaptive_min_temp = 21.0
        self.adaptive_max_temp = 37.0
        self.morphology_open_kernel_size = 2
        self.morphology_close_kernel_size = 5
        self.thresholding_constant = -30
        self.thresholding_block_size = 95
        self.box_min_area = 54
        self.box_min_temp = 25.0

        # processing images for debugging
        self.image_temp_mask = None
        self.image_threshold_mask = None
        self.image_threshold_mask_filtered = None
        self.image_detected = None

    def __str__(self) -> str:
        lines = ('DETECTOR:', '\t- TEMP: min: {} °C  max {} °C'.format(self.adaptive_min_temp, self.adaptive_max_temp),
                 '\t- KERNEL SIZE: open: {}  close: {}'.format(self.morphology_open_kernel_size, self.morphology_close_kernel_size),
                 '\t- THRESHOLDING: block size: {}  constant: {}'.format(self.thresholding_block_size, self.thresholding_constant),
                 '\t- BOX AREA: {}'.format(self.box_min_area), '\t- MIN BOX TEMP: {} °C'.format(self.box_min_temp))

        return '\n'.join(lines)

    def _filter_out_temperature_range(self, image: np.ndarray) -> np.ndarray:
        self.image_temp_mask = ThermoHelper.GetTemperatureMask(image, min_temp=self.adaptive_min_temp, max_temp=self.adaptive_max_temp, pixel_convertor=self.pixel_converter, verbose=self.verbose)
        temp_filter_inv = cv2.bitwise_not(self.image_temp_mask)

        # fill temperatures out of range with minimal temperature
        image_supplement = np.zeros_like(image)
        image_supplement.fill(max(image.min(), self.pixel_converter.Temp2Pixel(self.adaptive_min_temp)))
        image_supplement = cv2.bitwise_and(image, image, mask=temp_filter_inv)

        # merge in range areas and out of range areas (with reasonable values applied) of the image
        image_clipped = cv2.bitwise_and(image, image, mask=self.image_temp_mask)
        image_clipped = cv2.bitwise_or(image_clipped, image_supplement)
        return image_clipped

    def _filter_mask_noise(self, mask: np.ndarray) -> np.ndarray:
        kernel = np.ones((self.morphology_open_kernel_size, self.morphology_open_kernel_size), np.uint8)
        filtered_mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)

        kernel = np.ones((self.morphology_close_kernel_size, self.morphology_close_kernel_size), np.uint8)
        filtered_mask = cv2.morphologyEx(filtered_mask, cv2.MORPH_CLOSE, kernel)
        return filtered_mask

    def detect(self, image: np.ndarray) -> List[Tuple[int, int, int, int, float]]:
        image_clipped = self._filter_out_temperature_range(image)

        # normalize the dynamic range of the image
        image_normalized = ThermoHelper.Normalize_lin8(image_clipped)

        # applying adaptive thresholding
        self.image_threshold_mask = cv2.adaptiveThreshold(image_normalized, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, self.thresholding_block_size, self.thresholding_constant)

        # apply morphology transformations in order to filter noise
        self.image_threshold_mask_filtered = self._filter_mask_noise(self.image_threshold_mask)

        # find boxes surrounding detected objects
        potential_border_boxes = ThermoHelper.GetBoundingBoxes(self.image_threshold_mask_filtered, self.box_min_area)
        border_boxes = list()

        # draw bounding boxes
        self.image_detected = cv2.cvtColor(image_normalized, cv2.COLOR_GRAY2BGR)
        for box in potential_border_boxes:
            x, y, w, h, area = box

            # skip boxes with temp below threshold
            box_max_temp = self.pixel_converter.Pixel2Temp(image_clipped[y:y+h, x:x+w].max())
            if box_max_temp < self.box_min_temp:
                continue

            # draw
            border_boxes.append(box)
            cv2.rectangle(self.image_detected, (x, y), (x + w, y + h), (0, 0, 255), 1)

        return border_boxes