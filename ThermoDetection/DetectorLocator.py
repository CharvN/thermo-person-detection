#  ###########################################################################
 #  Copyright(C) 2020 Michal Charvat
 #
 #  This program is free software: you can redistribute it and/or modify
 #  it under the terms of the GNU Lesser General Public License as published by
 #  the Free Software Foundation, either version 3 of the License, or
 #  (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU Lesser General Public License for more details.
 #
 #  You should have received a copy of the GNU Lesser General Public License
 #  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#  ###########################################################################

import os
import cv2
from typing import Dict, List, Tuple
from matplotlib import pyplot as plt
import matplotlib.patches as mpatches

from ThermoDetection import ThermoHelper
from ThermoDetection.Scene import Scene
from ThermoDetection.Camera import Camera
from ThermoDetection.YoloDetector import YoloDetector

from v4l2lepton3.client import Lepton3Client

class FileDetectorLocator(object):
	def __init__(self, scene_config_path: str, camera_name: str, directory: str, raw=False):
		self._scene: Scene = Scene.from_json_config(scene_config_path)
		self._camera_name = camera_name
		self._directory = directory
		self._raw = raw

		assert camera_name in self._scene.cameras, 'Camera was not found in the scene: {}'.format(camera_name)
		assert self._scene.cameras[camera_name].is_calibrated(), 'Camera is not calibrated: {}'.format(camera_name)
		assert os.path.isdir(directory), 'Directory not found: {}'.format(directory)

		self._detector: YoloDetector = YoloDetector(160, 160, './dnn/yolov4-thermal-320.cfg',  './dnn/yolov4-thermal-320.weights')

		self._figure = None
		self._scene_plot = None
		self._directory_iterator = None

	def __enter__(self):
		# create camera windows
		cv2.namedWindow(self._camera_name, cv2.WINDOW_NORMAL)
		cv2.resizeWindow(self._camera_name, 160, 120)

		self._figure = plt.figure('Ground plan')
		self._scene_plot = self._figure.add_subplot(111)

		provider = ThermoHelper.ImageProvider(self._directory)
		self._directory_iterator = provider.iter_raw_normalized() if self._raw else provider.iter_regular()
		return self

	def __exit__(self, exc_type, exc_val, exc_tb):
		cv2.destroyAllWindows()
		plt.close(self._figure)
		self._figure = None
		self._scene_plot = None
		self._directory_iterator = None

	def draw_scene(self, detected_points: List[Tuple[int, int]]):
		# legend and background
		legend_handles = [mpatches.Patch(color='red', label='detected_objects')]
		self._scene_plot.clear()
		self._scene.draw_scene(self._scene_plot, legend_handles)

		# detected people
		for point in detected_points:
			self._scene_plot.plot(*point[:2], 'ro', color='red')

		# redraw canvas
		self._figure.canvas.draw()
		plt.pause(0.001)

	def new_frame(self):
		assert self._directory_iterator is not None, 'You must first init the DetectorLocator using the `with` statement.'
		file_path, _, image = next(self._directory_iterator)

		detected_people = list()
		boxes = self._detector.detect(image)
		for box in boxes:
			cv2.rectangle(image, box, color=(0, 255, 0), thickness=1)
			detected_people.append(ThermoHelper.GetWorldPosition(box, self._scene.cameras[self._camera_name]))

		self.draw_scene(detected_people)
		cv2.imshow(self._camera_name, image)

		key = cv2.waitKey(0) & 0xFF
		if key == 27:
			raise KeyboardInterrupt

		elif key == ord(' '):  # save
			path, extension = os.path.splitext(os.path.split(file_path)[1])
			image_path, scene_path = '{}_detected{}'.format(path, extension), '{}_scene{}'.format(path, extension)
			cv2.imwrite(image_path, image)
			self._figure.savefig(scene_path)
			print('{} + {} saved.'.format(image_path, scene_path))


class LiveDetectorLocator(object):
	def __init__(self, scene_config_path: str):
		self._scene: Scene = Scene.from_json_config(scene_config_path)

		self._calibrated_cameras: Dict[str, Camera] = {camera_name: camera for camera_name, camera in self._scene.cameras.items() if camera.is_calibrated() and camera.is_connectible()}
		assert len(self._calibrated_cameras) > 0, 'Scene does not contain any calibrated camera.'

		self._clients: Dict[str, Lepton3Client] = dict()
		self._detector: YoloDetector = YoloDetector(160, 160, './dnn/yolov4-thermal-320.cfg',  './dnn/yolov4-thermal-320.weights')

		self._figure = None
		self._scene_plot = None

	def __enter__(self):
		# create camera windows
		for camera_name, camera in self._calibrated_cameras.items():
			cv2.namedWindow(camera_name, cv2.WINDOW_NORMAL)
			cv2.resizeWindow(camera_name, 160, 120)

			self._clients[camera_name] = Lepton3Client(camera.ip, camera.port)
			self._clients[camera_name].__enter__()

			self._figure = plt.figure('Ground plan')
			self._scene_plot = self._figure.add_subplot(111)

		return self

	def __exit__(self, exc_type, exc_val, exc_tb):
		for client in self._clients.values():
			client.__exit__(exc_type, exc_val, exc_tb)
		self._clients = dict()

		cv2.destroyAllWindows()
		plt.close(self._figure)
		self._figure = None
		self._scene_plot = None

	def draw_scene(self, detected_points: List[Tuple[int, int]]):
		# legend and background
		legend_handles = [mpatches.Patch(color='red', label='detected_objects')]
		self._scene_plot.clear()
		self._scene.draw_scene(self._scene_plot, legend_handles)

		# detected people
		for point in detected_points:
			self._scene_plot.plot(*point[:2], 'ro', color='red')

		# redraw canvas
		self._figure.canvas.draw()
		plt.pause(0.001)

	def new_frame(self):
		detected_people = list()

		for camera_name, camera in self._calibrated_cameras.items():
			frame = self._clients[camera_name].get_frame()
			image = ThermoHelper.Normalize_lin8(ThermoHelper.Normalize_clahe(frame))
			image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)

			boxes = self._detector.detect(image)
			for box in boxes:
				cv2.rectangle(image, box, color=(0, 255, 0), thickness=1)
				detected_people.append(ThermoHelper.GetWorldPosition(box, camera))

			cv2.imshow(camera_name, image)

		cv2.waitKey(1)
		self.draw_scene(detected_people)