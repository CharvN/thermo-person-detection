#  ###########################################################################
 #  Copyright(C) 2020 Michal Charvat
 #
 #  This program is free software: you can redistribute it and/or modify
 #  it under the terms of the GNU Lesser General Public License as published by
 #  the Free Software Foundation, either version 3 of the License, or
 #  (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU Lesser General Public License for more details.
 #
 #  You should have received a copy of the GNU Lesser General Public License
 #  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#  ###########################################################################

import cv2
import numpy as np
import matplotlib.patches as mpatches

from collections import deque
from typing import Tuple, Union
from matplotlib import pyplot as plt

from ThermoDetection.Scene import Scene
from ThermoDetection import ThermoHelper
from ThermoDetection.Camera import Camera


class MappingPoint(object):
    def __init__(self, image_point: Tuple[int, int]):
        self.image_point = image_point
        self.world_point = None

    def is_complete(self) -> bool:
        return self.image_point is not None and self.world_point is not None

    def __str__(self) -> str:
        return ('{} -> {}'.format(self.image_point, self.world_point))


class CalibratorStatic(object):
    def __init__(self, scene: Scene, camera_name: str, image_path: Union[str, None]):
        assert camera_name in scene.cameras, 'Camera name not in the scene: {}'.format(camera_name)

        self.scene: Scene = scene
        self.image_path: Union[str, None] = image_path
        self.camera_name: str = camera_name
        self.camera: Camera = self.scene.cameras[camera_name]

        self.image: Union[np.ndarray, None] = None
        self.original_camera_color = None

        # load image
        if image_path:
            self.image = cv2.imread(self.image_path, cv2.CV_16U)
            assert self.image is not None, 'Failed to read scene image: {}'.format(self.image_path)
            assert self.image.shape[0] == self.camera.image_height and self.image.shape[1] == self.camera.image_width, 'Camera parameters dont match image dimensions.'
            self.image = ThermoHelper.Normalize_lin8(ThermoHelper.Normalize_clahe(self.image))

        # contrast
        self.beta = 0
        self.alpha = 1.0
        self.contrast_adjust_func = np.vectorize(lambda pixel: np.clip(self.alpha * pixel + self.beta, 0, 255), otypes=[np.uint8])

        # calibration points
        self.mapping_points = deque()

        # drawing
        self.figure = None
        self.scene_plot = None

    def __enter__(self):
        self.original_camera_color = self.camera.color
        self.camera.color = 'orange'
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.camera.color = self.original_camera_color
        plt.close(self.figure)
        cv2.destroyAllWindows()

    def get_image(self) -> np.ndarray:
        return self.image

    def print_controls(self):
        lines = ('\nControls:', '\t- UP - contrast down', '\t- DOWN - contrast down', '\t- RIGHT - brightness up', '\t- LEFT - brightness down',
                '\t- BACKSPACE - remove last mapping point', '\t- ESC - save and exit')
        print('\n'.join(lines))

    def print_mapping_points(self):
        print('Mapping keys (image) -> (world):\n{}'.format('\n'.join(str(mapping_point) for mapping_point in self.mapping_points)))

    def draw_scene(self, figure, plot):
        legend_handles = [mpatches.Patch(color='red', label='mapping_points')]
        self.scene.draw_scene(plot, legend_handles)

        for mapping_point in self.mapping_points:
            plot.plot(*mapping_point.world_point[:2], 'ro', color='red')

        # draw
        figure.canvas.draw()
        plt.pause(0.001)

    def draw_selected_mapping_points(self, image: np.ndarray):
        for mapping_point in self.mapping_points:
            image[mapping_point.image_point[1], mapping_point.image_point[0]] = (0, 0, 255)

    def has_uncomplete_mapping_point(self) -> bool:
        return len(self.mapping_points) > 0 and not self.mapping_points[-1].is_complete()

    def calibrate(self):
        self.create_mapping()

        assert len(self.mapping_points) >= 4
        assert not self.has_uncomplete_mapping_point()
        image_points = [mapping_point.image_point for mapping_point in self.mapping_points]
        world_points = [mapping_point.world_point for mapping_point in self.mapping_points]

        self.camera.scene_calibrate(world_points, image_points)

    def create_mapping(self):
        # draw a scene
        self.figure = plt.figure('Ground plan')
        self.scene_plot = self.figure.add_subplot(111)
        self.draw_scene(self.figure, self.scene_plot)

        # create window for
        window_name = 'Calibrating CAM: {}'.format(self.camera.name)
        cv2.namedWindow(window_name, cv2.WINDOW_NORMAL)
        cv2.resizeWindow(window_name, 160, 120)
        cv2.setMouseCallback(window_name, self.mouse_callback)

        # print controls and run edit loop
        self.print_controls()
        while(True):
            # draw image
            contrast_adjusted_image = self.contrast_adjust_func(self.get_image())
            contrast_adjusted_image = cv2.cvtColor(contrast_adjusted_image, cv2.COLOR_GRAY2BGR)
            self.draw_selected_mapping_points(contrast_adjusted_image)
            cv2.imshow(window_name, contrast_adjusted_image)

            # finish mapping
            if self.has_uncomplete_mapping_point():
                cv2.waitKey(1)

                while(True):
                    try:
                        # ask for a coordinate
                        world_point = input('Insert a corresponding 3D world coordinate for {}: (x: int, y: int, z: int). Ex.: `x,y,z`:'.format(self.mapping_points[-1].image_point))
                        world_point = tuple(int(x.strip()) for x in world_point.split(','))
                        assert len(world_point) == 3, 'Must have 3 coordinates'

                        # save world point
                        self.mapping_points[-1].world_point = world_point
                        break
                    except Exception as e:
                        print('Incorrect format: {}'.format(e))

                # redraw scene
                self.scene_plot.clear()
                self.draw_scene(self.figure, self.scene_plot)

                # mapping point added
                print('Added mapping point: {}'.format(self.mapping_points[-1]))
                self.print_mapping_points()

            # process hotkeys
            key = cv2.waitKey(100) & 0xFF

            # image adjustments
            if 81 <= key <= 84:
                # LEFT
                if key == 81:
                    self.beta -= 5
                # RIGHT
                elif key == 83:
                    self.beta += 5
                # UP
                elif key == 82:
                    self.alpha += 0.1
                # DOWN
                elif key == 84:
                    self.alpha -= 0.1
                print('Alpha: {}  Beta: {}'.format(self.alpha, self.beta))

            # BACKSPACE
            elif key == 8:
                if len(self.mapping_points) <= 0:
                    print('No mapping points to remove.')
                else:
                    # remove mapping points
                    self.mapping_points.pop()
                    self.print_mapping_points()

                    # redraw scene
                    self.scene_plot.clear()
                    self.draw_scene(self.figure, self.scene_plot)

            # ESC
            elif key == 27:
                if len(self.mapping_points) < 4 or self.has_uncomplete_mapping_point():
                    print('Minimum of 4 mapping points is required.')
                else:
                    break

    def mouse_callback(self, event, x, y, flags, param):
        if event == cv2.EVENT_LBUTTONDOWN:
            if self.has_uncomplete_mapping_point():
                print('Must first finish mapping before creating a new image point.')
                return

            image_point = (int(x), int(y))
            mapping_point = MappingPoint(image_point)
            self.mapping_points.append(mapping_point)


class CalibratorDynamic(CalibratorStatic):
    def __init__(self, scene: Scene, camera_name: str, ip: str, port: int):
        super(CalibratorDynamic, self).__init__(scene, camera_name, None)
        self._ip = ip
        self._port = port

    def __enter__(self):
        from v4l2lepton3.client import Lepton3Client
        self._client = Lepton3Client(self._ip, self._port)
        self._client.__enter__()
        return super(CalibratorDynamic, self).__enter__()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._client.__exit__(exc_type, exc_val, exc_tb)
        self._client = None
        super(CalibratorDynamic, self).__exit__(exc_type, exc_val, exc_tb)

    def get_image(self) -> np.ndarray:
        image = self._client.get_frame()
        image = ThermoHelper.Normalize_lin8(ThermoHelper.Normalize_clahe(image))
        return image