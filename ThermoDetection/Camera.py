#  ###########################################################################
 #  Copyright(C) 2020 Michal Charvat
 #
 #  This program is free software: you can redistribute it and/or modify
 #  it under the terms of the GNU Lesser General Public License as published by
 #  the Free Software Foundation, either version 3 of the License, or
 #  (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU Lesser General Public License for more details.
 #
 #  You should have received a copy of the GNU Lesser General Public License
 #  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#  ###########################################################################

import cv2
import numpy as np
from math import sqrt
from typing import Union, Tuple, List


class Camera(object):
    def __init__(self, camera_position: Tuple[int, int, int], name: str, ip: str = None, port: int = 2222, color: str = 'green',
                 width: int = 160, height: int = 120, horizontal_fov: float = 56.0, vertical_fov: float = 42.0):
        '''
        Lepton3.5 camera constructor.
        :param camera_position: tuple (x_coord, y_coord, z_coord) (int, int, int) coordinates of the camera in the scene
        :param width: int image
        :param height: int image height
        :param horizontal_fov: Horizontal field of view in degrees (full angle) double
        :param vertical_fov: Vertical field of view in degrees (full angle) double
        :param color: color of the camera for displaying
        :param name: name of the camera for displaying
        :param ip: ip of the lepton3server for live feed
        :param port: port of the lepton3server
        '''
        assert len(camera_position) == 3
        assert all(type(coord) == int for coord in camera_position)

        # scene variables
        self.ip: str = ip
        self.port: int = port
        self.name: str = name
        self.color: str = color
        self.image_width: int = width
        self.image_height: int = height
        self.vertical_fov: float = vertical_fov
        self.horizontal_fov: float = horizontal_fov
        self.camera_position: np.ndarray = np.asarray([*camera_position, 1.0], dtype=np.double)

        # construct intrinsic camera matrix, distortion coefficients
        self.dist_coefficients: Union[np.ndarray, None] = None # assume no distortion
        self.intrinsic_camera_mat: Union[np.ndarray, None] = None
        self.inverse_intrinsic_camera_mat: Union[np.ndarray, None] = None
        self.setup_intrinsic_camera_matrices()

        # camera pose variables
        self.rotation_vec: Union[np.ndarray, None] = None
        self.translation_vec: Union[np.ndarray, None] = None
        self.world_to_camera_mat: Union[np.ndarray, None] = None
        self.camera_to_world_mat: Union[np.ndarray, None] = None
        self.world_camera_position: Union[np.ndarray, None] = None

        # reference calibration points
        self.image_points: Union[np.ndarray, None] = None
        self.world_points: Union[np.ndarray, None] = None

    def __str__(self) -> str:
        string = 'CAM: {}, {}, {}:{}. {}, intrinsic_mat={}, scene_calibrated={}'.format(
            self.name, self.color, self.ip, self.port,
            tuple(self.world_camera_position[:3].astype(float)) if self.is_calibrated() else tuple(self.camera_position[:3].astype(int)),
            self.inverse_intrinsic_camera_mat is not None,
            self.is_calibrated())
        return string

    def is_calibrated(self) -> bool:
        return self.camera_to_world_mat is not None

    def is_connectible(self) -> bool:
        return self.ip is not None

    def setup_intrinsic_camera_matrices(self):
        '''
        Constructs intrinsic camera matrix, distortion coefficients. Default values correspond to the Lepton3 camera. Assumed no distortion.
        '''

        horizontal_fov = np.deg2rad(self.horizontal_fov)
        vertical_fov = np.deg2rad(self.vertical_fov)

        center_x =  self.image_width // 2
        center_y =  self.image_height // 2

        fx = center_x / np.tan(horizontal_fov / 2)  # for Lepton3s 56deg horizontal fov should be 150.45812
        fy = center_y / np.tan(vertical_fov / 2)  # for Lepton3s 42deg vertical fov should be 156.30534

        self.intrinsic_camera_mat = np.array(
            [[fx, 0, center_x],
             [0, fy, center_y],
             [0, 0, 1]], dtype=np.double
        )

        # compute inverse of the intrinsic camera matrix
        self.inverse_intrinsic_camera_mat = np.linalg.inv(self.intrinsic_camera_mat)

    def scene_calibrate(self, world_points: List[Tuple[int, int, int]], image_points: List[Tuple[int, int]]):
        '''
        Constructs the scene, estimates camera position in the scene, prepares necessary matrices to perform reverse projecting pixels into the scene.
        Necessary step before projecting image pixels.
        :param image_points: [(x1, y1), (x2, y2), ...] set of 2D image poins to be matched with 3D points in world space
        :param world_points: [(x1, y1, z1), (x2, y2, z2), ...] set of 3D world poins to be matched with 2D points in image space
        '''
        assert len(image_points) == len(world_points)
        assert all(len(point) == 2 for point in image_points)
        assert all(len(point) == 3 for point in world_points)
        assert self.intrinsic_camera_mat is not None

        image_points = np.asarray(image_points, dtype=np.double)
        world_points = np.asarray(world_points, dtype=np.double)

        # solve perspective-n-point problem, camera pose - get rotation vector, translation vector
        flags = cv2.SOLVEPNP_DLS
        success, self.rotation_vec, self.translation_vec = cv2.solvePnP(world_points, image_points, self.intrinsic_camera_mat, self.dist_coefficients, None, self.camera_position, True, flags)
        # success, self.rotation_vec, self.translation_vec, _ = cv2.solvePnPRansac(list_3d_points, list_2d_points, self.intrinsic_camera_mat, self.dist_coefficients)

        if not success:
            raise ValueError("Unable to solve PnP.")

        # get rotation matrix, inverse rotation matrix
        rotation_mat, _ = cv2.Rodrigues(self.rotation_vec)
        inverse_rotation_mat = rotation_mat.T

        # construct world to camera matrix - forward transformation
        self.world_to_camera_mat = np.zeros((4, 4), dtype=np.double)
        self.world_to_camera_mat[3, 3] = 1.0
        self.world_to_camera_mat[0:3, 0:3] = rotation_mat
        self.world_to_camera_mat[0:3, 3:4] = self.translation_vec

        # construct camera to world matrix - reverse transformation
        self.camera_to_world_mat = np.zeros((4, 4), dtype=np.double)
        self.camera_to_world_mat[3, 3] = 1.0
        self.camera_to_world_mat[0:3, 0:3] = inverse_rotation_mat
        self.camera_to_world_mat[0:3, 3:4] = -np.dot(inverse_rotation_mat, self.translation_vec)

        # reverse project camera origin into world space
        self.world_camera_position = np.dot(self.camera_to_world_mat, np.array([0, 0, 0, 1]).T)

        # store calibration points for export
        self.image_points = image_points.astype(int)
        self.world_points = world_points.astype(int)

    def reverse_project_zcoord(self, point_image: Tuple[int, int], z_coordinate: float) -> np.ndarray:
        '''
        Reverse projects image point to the 3D world space with a known z coordinate.
        :param point_image: image point (x, y) to be reverse projected to world space of the scene
        :param z_coordinate: z coordinate of the point we are looking for in the world space. (Needed in order to collapse the casted ray)
        :return: returns reverse projected point (x, y, z) in world space.
        '''
        assert self.camera_to_world_mat is not None
        assert len(point_image) == 2

        # project point from image space to camera space
        point_camera = np.dot(self.inverse_intrinsic_camera_mat, np.array([*point_image, 1.0], dtype=np.double).T)

        # extend point to 4D (homogeneous coordinates)
        point_camera = np.array([*point_camera, 1.0])

        # project point from camera point to world space
        point_world = np.dot(self.camera_to_world_mat, point_camera.T)

        # cast a ray from camera origin through a the point (both in world space)
        ray = point_world - self.world_camera_position

        # collapse the ray to a 3D point in known Z coordinate
        try:
            scalar = (z_coordinate - self.world_camera_position[2]) / ray[2]
            point = self.world_camera_position + scalar * ray
        except ZeroDivisionError:
            raise ValueError('The ray does not collapse to a single point in world space.')

        point = point[:3]
        return point

    def get_world_fov_points(self, length: Union[int, float] = 200) -> List[np.ndarray]:
        '''
        Computes two image coordinates that correspond to end points of a FOV segments leading from the camera position with length specified
        :param length: length of the FOV segment in world space
        :return: list of two points in image space that correspond to endpoints of segments showing FOV cone
        '''
        assert self.is_calibrated(), 'Camera must be calibrated before computing FOV cone'

        world_side_points = list()
        image_side_points = np.array([
            [0.0, self.image_height / 2.0, 1.0],
            [self.image_width, self.image_height / 2.0, 1.0]
        ], dtype=np.double)


        for point_image in image_side_points:
            # project point from image space to camera space
            point_camera = np.dot(self.inverse_intrinsic_camera_mat, point_image.T)

            # extend point to 4D (homogeneous coordinates)
            point_camera = np.array([*point_camera, 1.0])

            # project point from camera point to world space
            point_world = np.dot(self.camera_to_world_mat, point_camera.T)

            # cast a ray from camera origin through a the point (both in world space)
            ray = point_world - self.world_camera_position

            scalar = sqrt(length**2 / (ray[0]**2 + ray[1]**2)) # we ignore Z coordinate, we want the displayed fov to be equally long from top perspective
            point = self.world_camera_position + scalar * ray
            world_side_points.append(point)

        return world_side_points

    def serialize(self) -> dict:
        camera = {
            'name': self.name,
            'color': self.color,
            "position": self.camera_position[:3].astype(np.int).tolist()
        }

        if self.ip:
            camera.update({
                'ip': self.ip,
                'port': self.port,
            })

        if self.is_calibrated():
            camera.update({
                'mapping_points': {
                    'image_points': self.image_points.tolist(),
                    'world_points': self.world_points.tolist()
                }
            })

        return camera