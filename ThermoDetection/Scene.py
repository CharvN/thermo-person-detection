#  ###########################################################################
 #  Copyright(C) 2020 Michal Charvat
 #
 #  This program is free software: you can redistribute it and/or modify
 #  it under the terms of the GNU Lesser General Public License as published by
 #  the Free Software Foundation, either version 3 of the License, or
 #  (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU Lesser General Public License for more details.
 #
 #  You should have received a copy of the GNU Lesser General Public License
 #  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#  ###########################################################################

import json
import matplotlib.patches as mpatches

from matplotlib import pyplot as plt
from typing import TypeVar, Dict, Tuple

from ThermoDetection.Camera import Camera
from ThermoDetection.Boundary import Boundary

TScene = TypeVar('TScene', bound='Scene')

class Scene(object):
	def __init__(self):
		'''
		Scene constructor. After calling this, setup the scene by calling:
		set_camera_position, set_intrinsic_camera_parameters, add_scene_vertices, scene_calibrate.
		Reverse project pixels to the scene using: reverse_project_zcoord.
		Draw scene using: draw_scene.
		'''
		# scene variables
		self.cameras: Dict[str, Camera] = dict()
		self.boundaries: Dict[str, Boundary] = dict()

	@staticmethod
	def from_json_config(config_path: str) -> TScene:
		scene = Scene()
		with open(config_path, 'r') as config_file:
			scene_config = json.load(config_file)

		# construct boundaries
		for boundary in scene_config['scene']['boundaries']:
			boundary_name = boundary['name']
			boundary_color = boundary['color']
			vertices = [tuple(coords) for coords in boundary['vertices']]

			assert boundary_name not in scene.boundaries, 'Conflicting boundary names.'
			assert all(len(coords) == 2 for coords in vertices), 'Boundary vertices must have 2 coordinates.'
			assert all(all(type(coord) == int for coord in coords) for coords in vertices), 'Boundary vertex has to be in (int, int) format.'

			scene.boundaries[boundary_name] = Boundary(boundary_name, vertices, boundary_color)

		# create cameras
		for camera in scene_config['scene']['cameras']:
			camera_name = camera['name']
			camera_color = camera['color']
			camera_position = tuple(camera['position'])

			camera_ip = camera.get('ip')
			camera_port = camera.get('port', 2222)

			assert camera_name not in scene.cameras, 'Conflicting camera names.'
			assert len(camera_position) == 3 and all(type(coord) == int for coord in camera_position), 'Camera position has to be in (int, int, int) format.'

			scene_camera = Camera(camera_position, camera_name, camera_ip, camera_port, camera_color)
			scene.cameras[camera_name] = scene_camera

			if camera.get('mapping_points'):
				try:
					image_points = camera['mapping_points']['image_points']
					world_points = camera['mapping_points']['world_points']
					scene_camera.scene_calibrate(world_points, image_points)
				except Exception as e:
					print('Failed calibrating camera from mapping points from config: {}'.format(e))

		return scene

	def get_min_max_boundaries(self) -> Tuple[int, int, int, int]:
		xmin = min(boundary.xmin for boundary in self.boundaries.values())
		xmax = max(boundary.xmax for boundary in self.boundaries.values())
		ymin = min(boundary.ymin for boundary in self.boundaries.values())
		ymax = max(boundary.ymax for boundary in self.boundaries.values())
		return xmin, xmax, ymin, ymax

	def is_out_of_boundary(self, point: Tuple[int, int]) -> bool:
		# compute global boundaries
		xmin, xmax, ymin, ymax = self.get_min_max_boundaries()

		x, y = point[:2]
		if not (xmin <= x <= xmax):
			return True
		if not (ymin <= y <= ymax):
			return True
		return False

	def __str__(self) -> str:
		string = 'Cameras:'
		for camera in self.cameras.values():
			string = '{}\n\t- {}'.format(string, camera)

		string += '\nBoundaries:'
		for boundary in self.boundaries.values():
			string = '{}\n\t- {}'.format(string, boundary)

		return string

	def draw_scene(self, plot=None, handles=None):
		'''
		Draws configured scene. Draws borders, camera position and reverse projected vertices (image pixels to 3D).
		'''
		plot = plot if plot is not None else plt
		legend_handles = list() if not handles else handles

		# draw boundaries
		for boundary in self.boundaries.values():
			legend_handles.append(mpatches.Patch(color=boundary.color, label=boundary.name))
			self._draw_lines(plot, boundary.vertices, boundary.color)

		# draw camera position
		for camera in self.cameras.values():
			legend_handles.append(mpatches.Patch(color=camera.color, label=camera.name))
			camera_position = camera.world_camera_position if camera.is_calibrated() else camera.camera_position
			plot.plot(*camera_position[:2], 'ro', color=camera.color)

			if camera.is_calibrated():
				world_fov_points = camera.get_world_fov_points(200)
				for world_fov_point in world_fov_points:
					self._draw_line(plot, camera.world_camera_position, world_fov_point, color=camera.color)

		# draw legend
		plot.legend(handles=legend_handles)

	def _draw_lines(self, plot, vertices, color='blue'):
		for i, vertex1 in enumerate(vertices[:-1]):
			vertex2 = vertices[i + 1]
			self._draw_line(plot, vertex1, vertex2, color)

	def _draw_line(self, plot, vertex1, vertex2, color):
		plot.plot((vertex1[0], vertex2[0]), (vertex1[1],vertex2[1]), color=color)

	def save_json_config(self, config_path: str):
		scene_json = {
			'scene': {
				'boundaries': [boundary.serialize() for boundary in self.boundaries.values()],
				'cameras': [camera.serialize() for camera in self.cameras.values()]
			}
		}

		scene_json_str = json.dumps(scene_json, indent=4)
		with open(config_path, 'w') as config_file:
			config_file.write(scene_json_str)