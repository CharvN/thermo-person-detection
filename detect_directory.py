#!/usr/bin/env python3

#  ###########################################################################
 #  Copyright(C) 2020 Michal Charvat
 #
 #  This program is free software: you can redistribute it and/or modify
 #  it under the terms of the GNU Lesser General Public License as published by
 #  the Free Software Foundation, either version 3 of the License, or
 #  (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU Lesser General Public License for more details.
 #
 #  You should have received a copy of the GNU Lesser General Public License
 #  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#  ###########################################################################

import argparse
from ThermoDetection.DetectorLocator import FileDetectorLocator

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Script to find proper hyperparameters for thermal detection')
	parser.add_argument('-s', '--scene', dest='scene', required=True, help='Scene model with calibrated camera in .JSON format.')
	parser.add_argument('-c', '--camera', dest='camera', required=True, help='Camera name.')
	parser.add_argument('-d', '--dir', dest='directory', required=True, help='Directory of .jpg files (or .tiff if -r is used).')
	parser.add_argument('-r', '--raw', dest='raw', action='store_true', help='Looks for .tiff raw temp files instead of .jpg.')
	args = parser.parse_args()

	try:
		locator = FileDetectorLocator(args.scene, args.camera, args.directory, args.raw)
		with locator:
			while True:
				locator.new_frame()

	except StopIteration:
		print('Done.')

	except KeyboardInterrupt:
		pass