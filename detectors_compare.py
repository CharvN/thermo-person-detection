#!/usr/bin/env python3

#  ###########################################################################
 #  Copyright(C) 2020 Michal Charvat
 #
 #  This program is free software: you can redistribute it and/or modify
 #  it under the terms of the GNU Lesser General Public License as published by
 #  the Free Software Foundation, either version 3 of the License, or
 #  (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU Lesser General Public License for more details.
 #
 #  You should have received a copy of the GNU Lesser General Public License
 #  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#  ###########################################################################

import os
import cv2
from sys import argv
from itertools import chain

from ThermoDetection import ThermoHelper
from ThermoDetection.YoloDetector import YoloDetector
from ThermoDetection.ThermoDetector import ThermoDetector


if __name__ == '__main__':
    assert len(argv) == 2, 'Usage ./detectors_compare.py <directory>\n\t<directory> -- path to a directory with .tiff raw thermal images'

    directory = argv[1]
    assert os.path.isdir(directory), 'Argument is not a directory: {}'.format(directory)

    raw_detectors = {
        'legacy': ThermoDetector(),
    }
    normalized_detectors = {
        'yolo3-160': YoloDetector(160, 160, './dnn/yolov3-spp-thermal-160.cfg',  './dnn/yolov3-spp-thermal-160.weights'),
        'yolo3-320': YoloDetector(160, 160, './dnn/yolov3-spp-thermal-320.cfg', './dnn/yolov3-spp-thermal-320.weights'),
        'yolo4-320': YoloDetector(160, 160, './dnn/yolov4-thermal-320.cfg',  './dnn/yolov4-thermal-320.weights'),
        'yolo4-416': YoloDetector(160, 160, './dnn/yolov4-thermal-416.cfg',  './dnn/yolov4-thermal-416.weights'),
        'yolo4-512': YoloDetector(160, 160, './dnn/yolov4-thermal-512.cfg',  './dnn/yolov4-thermal-512.weights'),
    }

    for window_name in chain(raw_detectors.keys(), normalized_detectors.keys()):
        cv2.namedWindow(window_name, cv2.WINDOW_NORMAL)
        cv2.resizeWindow(window_name, 160, 120)

    try:
        for image_path, frame, image in ThermoHelper.ImageProvider(directory).iter_raw_normalized():
            print('File: {}'.format(image_path))

            boxed_images = dict()
            for detector_name, detector in raw_detectors.items():
                boxes = detector.detect(frame)
                image_boxes = image.copy()

                for box in boxes:
                    cv2.rectangle(image_boxes, box[:4], color=(255, 0, 0), thickness=1)

                boxed_images[detector_name] = image_boxes
                cv2.imshow(detector_name, image_boxes)

            for detector_name, detector in normalized_detectors.items():
                boxes = detector.detect(image)
                image_boxes = image.copy()

                for box in boxes:
                    cv2.rectangle(image_boxes, box[:4], color=(255, 0, 0), thickness=1)

                boxed_images[detector_name] = image_boxes
                cv2.imshow(detector_name, image_boxes)

            key = cv2.waitKey(0) & 0xFF
            if key == 27:
                raise KeyboardInterrupt

            elif key == ord(' '):
                # save
                image_name, extension = os.path.splitext(os.path.split(image_path)[1])

                for window_name, image in boxed_images.items():
                    save_path = '{}_{}.png'.format(image_name, window_name)
                    cv2.imwrite(save_path, image)
                    print('{} saved.'.format(save_path))

    except KeyboardInterrupt:
        pass