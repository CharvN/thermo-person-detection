# System for People Detection and Localization Using Thermal Imaging Cameras

## Master's thesis 2020
This repository is the main repository for the system for detection and localization. It contains the text of the Master's thesis, a supporting Python `ThermoDetection` library, YOLO thermal detector models, a detector abstraction,
a scene abstraction, a visual scene calibrator, the __online detection and localization system__ and a plenty of supporting executable scripts. 


__Abstract__. In today's world, there is an increasing need for automatic reliable mechanisms for detecting and localizing people -- from performing people flow analysis in museums, controlling smart homes to guarding hazardous areas like railway platforms. We propose a method for detecting and locating people using low-cost FLIR Lepton 3.5 thermal cameras and a Raspberry Pi 3B+ computers. This thesis describes the continuation of the _Detection of People in Room Using Low-Cost Thermal Imaging Camera_ project, which now supports modelling of complex scenes with polygonal boundaries and multiple thermal cameras observing them. In this paper, we introduce an improved control and capture library for the Lepton 3.5, a new person detection technique that uses the state-of-the-art YOLO (You Only Look Once) real-time object detector based on deep neural networks, furthermore, a new thermal unit with automated configuration using Ansible encapsulated in a custom 3D printed enclosure for safe manipulation, and last but not least a step by step instruction manual on how to deploy the detection system in a new environment including other supporting tools and improvements. Results of the new system are demonstrated on a simple people flow analysis performed in the Czech National Museum in Prague.


## Content of this repository
#### Thesis
The thesis can be found in the `Thesis` directory in `.pdf` and `.tex` format. The Master's project is a continuation of the Bachelor's project, whose files can be found in the `bp` branch.

#### Thermo Detection
The core of the second programming part of the Master's project can be found in the `ThermoDetection` directory, it contains the helper Python3 library with the following classes:
- `ThermoHelper` class - contains helper functions for performing person detection from a thermal image.
- `Scene` class representing the scene abstraction providing functionality for locating people by reverse projecting pixels into the scene model.
- `Camera` class represents a single Lepton module. Contains projection matrices and helps with reverse projecting its image points.
- `Boundary`represents a single polygonal boundary in the scene with other properties.
- `CalibratorStatic`/`CalibratorDynamic` allows for visually calibrating each camera in a scene.
- `ThermoDetector` contains the implementation of the old simple legacy thermal detector based on filtering and thresholding.
- `YoloDetector` contains the implementation of the new YOLO person detector.
- `FileDetectorLocator` performs full detection and localization of people in a known environment based on pre-stored images and from a single camera.
- `LiveDetectorLocator` is the live implementation of the system that takes a scene configuration file, connects to all calibrated cameras, starts receiving frames
on which it performs detection and re-projects the detected objects into the scene.

See the thesis for more detailed description and a manual on how to setup the system in a new environment.

#### Executable scripts
The directory also contains scripts that make use of the `ThermoDetection` Python3 package:
- `lepton3client_detect.py` - Extended version of the Python3 v4l2lepton3 client that performs detection using the trained YOLO model.
- `calibrate_scene.py` - Visual scene calibrator. Allows to insert image to world mapping points and therefore construct a projection matrix for each camera.
- `calibrate_thermo_detection_parameters.py` - Script allowing for visual adjustments of the legacy `ThermoDetector`'s hyperparameters.
- `detectors_compare.py` - Script that uses all YOLOv4, the legacy detector and some YOLOv3 pre-trained models and runs detection on all images in a directory showing the direct performance comparison.
- `detect_directory.py` - Runs the detection and localization on all thermal images from a directory one by one and shows detected persons in both the thermal image and on the ground plan of the scene.
- `detect_live.py` - Loads the scene config, connects to each calibrated camera using v4l2lepton3 library and starts detecting and locating in each thermal feed and merges the detections in the same scene model.
- `heat_map_merger` - Takes detection points from a json and merges them in a loaded scene, draws all detections and constructs a heatmap.


#### YOLO thermal models
The `dnn` directory contains only config files for all trained YOLO models. The `.weights` files are too big and can be downloaded from [onedrive](??).
The following models were retrained to detect persons on thermal data: yolov3-tiny-160, yolov3-tiny-320, yolov3-spp-160, yolov3-spp-320,
yolov4-320, yolov4-416, yolov4-512. By default, __the yolov4-320__ is used as it behaves the best with the Lepton 3.5 thermal camera.

##v4l2lepton3 - Lepton 3.5 capture and control library
This repository contains the second part of the Master's thesis including the paper.
The first programming part regarding camera communication -- the __v4l2lepton3__ library can be found [here](https://gitlab.com/CharvN/v4l2lepton3).

## Usage
```text
./lepton3client.py <ip> <port>
    <ip> -- required, e.g. `192.168.1.2`
    <port> -- optional, default `2222`
```
```text
./calibrate_scene.py [-h] -s SCENE [-c CAMERA] [-f CALIBRATION_FILE]
    -h, --help -- show this help message and exit
    -s SCENE, --scene SCENE -- Path to a scene .JSON config file.
    -c CAMERA, --camera CAMERA -- Camara name to be calibrated.
    -f CALIBRATION_FILE, --file CALIBRATION_FILE -- Calibration .TIFF file to be used for camera calibrating.
```
```text
./calibrate_thermo_detection_params.py [-h] -d DIR [-s SCENE] [-c CAMERA]
    -h, --help -- show this help message and exit
    -d DIR, --dir DIR -- Directory containing .tiff files
    -s SCENE, --scene SCENE -- Scene model with calibrated camera in .JSON format.
    -c CAMERA, --camera CAMERA -- Camera name that is calibrated in the loaded scene.
```
```text
./detectors_compare.py <directory>
    <directory> -- path to a directory with .tiff raw thermal images
```
```text
./detect_directory.py [-h] -s SCENE -c CAMERA -d DIRECTORY [-r]
    -h, --help -- show this help message and exit
    -s SCENE, --scene SCENE -- Scene model with calibrated camera in .JSON format.
    -c CAMERA, --camera CAMERA -- Camera name.
    -d DIRECTORY, --dir DIRECTORY -- Directory of .jpg files (or .tiff if -r is used).
    -r, --raw --Looks for .tiff raw temp files instead of .jpg.
```
```text
./detect_live.py [-h] -s SCENE
    -h, --help -- show this help message and exit
    -s SCENE, --scene SCENE -- Scene model with calibrated camera in .JSON format.
```
```text
./heat_map_merger.py [-h] [-d DIR] -s SCENE [-c CAMERA] -j JSON [--draw] [--heatmap]
    -h, --help -- show this help message and exit
    -d DIR, --dir DIR -- Directory containing .tiff files.
    -s SCENE, --scene SCENE -- Scene model with calibrated camera in .JSON format.
    -c CAMERA, --camera CAMERA -- Camera name that is calibrated in the loaded scene.
    -j JSON, --json JSON --  Saved projected points into the scene. Will be loaded and overwritten with new JSON with merged points.
    --draw -- Should draw result (takes time).
    --heatmap -- Should draw a heatmap.
```