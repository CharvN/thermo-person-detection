---
# LOCALES TIMEZONES
- name: Set cs_CZ.UTF-8 and en_US.UTF-8 to be generated
  become: true
  lineinfile:
    path: /etc/locale.gen
    line: "{{ item }}"
  with_items:
    - "cs_CZ.UTF-8 UTF-8"
    - "en_US.UTF-8 UTF-8"
  register: locale_gen

- name: Set en_US.UTF-8 as system default
  become: true
  replace:
    path: /etc/default/locale
    regexp: '^\s*LANG=\S*'
    replace: 'LANG=en_US.UTF-8'
  register: lang_set

- name: Reconfigure system locale
  become: true
  shell: "dpkg-reconfigure -f noninteractive locales"
  when: (locale_gen.changed) or (lang_set.changed)

- name: Change timezone to Europe/Prague
  become: true
  shell: "raspi-config nonint do_change_timezone Europe/Prague"
  when: (locale_gen.changed) or (lang_set.changed)

# INSTALL PACKAGES
- name: Running apt update
  become: true
  apt:
    update_cache: yes
    cache_valid_time: 86400

- name: Running apt upgrade
  become: true
  apt:
    upgrade: yes
    update_cache: yes
    cache_valid_time: 86400

- name: Install apt packages.
  become: true
  apt:
    name: "{{ item }}"
    state: present
  with_items:
      - git
      - cmake
      - libomp-dev
      - libboost-all-dev
      - python-dev
      - python3-dev
      - python-pip
      - python3-pip
      - wiringpi
      - python-opencv
      - python3-opencv

- name: Install python3 modules.
  pip:
    name: "{{ item }}"
    state: present
    executable: pip3
  with_items:
      - smbus2
      - git+https://github.com/lthiery/SPI-Py.git
      - bluepy

- name: Install python2 modules.
  pip:
    name: "{{ item }}"
    state: present
  with_items:
      - smbus2
      - git+https://github.com/lthiery/SPI-Py.git

- name: Prepare lines for disabling Wi-Fi & Bluetooth into the config
  become: true
  lineinfile:
    path: /boot/config.txt
    line: "{{ item }}"
  with_items:
    - "#dtoverlay=pi3-disable-bt"
    - "#dtoverlay=pi3-disable-wifi"

- name: Remove dependencies that are no longer required
  become: true
  apt:
    autoremove: yes
    autoclean: yes