#!/usr/bin/env python3

#  ###########################################################################
 #  Copyright(C) 2020 Michal Charvat
 #
 #  This program is free software: you can redistribute it and/or modify
 #  it under the terms of the GNU Lesser General Public License as published by
 #  the Free Software Foundation, either version 3 of the License, or
 #  (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU Lesser General Public License for more details.
 #
 #  You should have received a copy of the GNU Lesser General Public License
 #  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#  ###########################################################################

import cv2

from typing import List, Tuple
from matplotlib import pyplot as plt
import matplotlib.patches as mpatches

from ThermoDetection.Scene import Scene
from ThermoDetection import ThermoHelper
from ThermoDetection.ThermoDetector import ThermoDetector

def print_controls():
    lines = ('\nControls:', '\t- UP/DOWN - adaptive temp MAX', '\t- LEFT/RIGHT - adaptive temp MIN',
             '\t- Q/A - thresholding BLOCK SIZE', '\t- W/S - thresholding CONSTANT', '\t- E/D - kernel size OPEN',
             '\t- R/F - kernel size CLOSE', '\t- T/G - min box area',
             '\t- U/J - min BOX TEMP threshold',
             '\t- SPACE - next image', '\t- ESC - exit and print parameters')
    print('\n'.join(lines))


def parse_args():
    import argparse
    parser = argparse.ArgumentParser(description='Script to find proper hyperparameters for thermal detection')
    parser.add_argument('-d', '--dir', required=True, help='Directory containing .tiff files')
    parser.add_argument('-s', '--scene', required=False, help='Scene model with calibrated camera in .JSON format.')
    parser.add_argument('-c', '--camera', required=False, help='Camera name that is calibrated in the loaded scene.')

    args = parser.parse_args()
    return args


def draw_scene(figure: plt.Figure, plot: plt.Subplot, scene: Scene, detected_points: List[Tuple[int, int]]):
    legend_handles = [mpatches.Patch(color='red', label='detected_objects')]
    scene.draw_scene(plot, legend_handles)

    for point in detected_points:
        plot.plot(*point[:2], 'ro', color='red')

    # draw
    figure.canvas.draw()
    plt.pause(0.001)


def main():
    args = parse_args()
    detector = ThermoDetector()

    # create drawing windows
    window_name_calibrator = 'Thermal Detection Hyperparameter Calibrator'
    cv2.namedWindow(window_name_calibrator, cv2.WINDOW_NORMAL)
    print_controls()

    # prepare scene
    if args.scene:
        assert args.camera, 'With -s you need to specify which camera should be used for projecting using -c'
        scene = Scene.from_json_config(args.scene)

        assert args.camera in scene.cameras, 'Unrecognized camera: {}'.format(args.camera)
        camera = scene.cameras[args.camera]
        assert camera.is_calibrated(), 'Camera is not calibrated: {}'.format(args.camera)
        camera.color = 'lime'

        # draw scene
        figure = plt.figure('Ground plan')
        scene_plot = figure.add_subplot(111)
        draw_scene(figure, scene_plot, scene, [])

    # iterate over .raw files in directory
    should_exit = False
    for file_name, frame, _ in ThermoHelper.ImageProvider(args.dir).iter_raw():
        if should_exit:
            break

        print('File: {}'.format(file_name))

        # loop adjusting hyper parameters
        detector.verbose = True
        while True:
            try:
                # perform detection
                detected_objects = detector.detect(frame)
                detector.verbose = False # verbose only for first detection of a file

                # draw
                borders = (5,) * 4
                border_color = (255,) * 3

                image_temp_mask = cv2.cvtColor(detector.image_temp_mask, cv2.COLOR_GRAY2BGR)
                image_threshold_mask = cv2.cvtColor(detector.image_threshold_mask, cv2.COLOR_GRAY2BGR)
                image_threshold_mask_filtered = cv2.cvtColor(detector.image_threshold_mask_filtered, cv2.COLOR_GRAY2BGR)

                image_temp_mask = cv2.copyMakeBorder(image_temp_mask, *borders, cv2.BORDER_CONSTANT, value=border_color)
                image_threshold_mask = cv2.copyMakeBorder(image_threshold_mask, *borders, cv2.BORDER_CONSTANT, value=border_color)
                image_threshold_mask_filtered = cv2.copyMakeBorder(image_threshold_mask_filtered, *borders, cv2.BORDER_CONSTANT, value=border_color)
                image_detected = cv2.copyMakeBorder(detector.image_detected, *borders, cv2.BORDER_CONSTANT, value=border_color)

                horizontal_concat1 = cv2.hconcat((image_threshold_mask, image_threshold_mask_filtered))
                horizontal_concat2 = cv2.hconcat((image_temp_mask, image_detected))

                final_image = cv2.vconcat((horizontal_concat1, horizontal_concat2))
                cv2.imshow(window_name_calibrator, final_image)

                # project detected objects
                if args.scene:
                    scene_plot.clear()
                    height, width = frame.shape[:2]
                    for x, y, w, h, area in detected_objects:
                        # use head
                        if x <= 0 or x + w >= width or y + h >= height:
                            point = camera.reverse_project_zcoord((x + w // 2, y), 170)
                        # use feet
                        else:
                            point = camera.reverse_project_zcoord((x + w // 2, y + h), 0)

                        if not scene.is_out_of_boundary(point):
                            scene_plot.plot(*point[:2], 'ro', color='red')

                    draw_scene(figure, scene_plot, scene, [])

            except:
                import traceback
                traceback.print_exc()


            # handle key press, adjust detection
            key = cv2.waitKey(0) & 0xFF
            if key in {ord('q'), ord('a'), ord('w'), ord('s')}:
                if key == ord('q'):
                    detector.thresholding_block_size += 2
                elif key == ord('a'):
                    detector.thresholding_block_size -= 2
                elif key == ord('w'):
                    detector.thresholding_constant += 2
                elif key == ord('s'):
                    detector.thresholding_constant -= 2
                print('THRESHOLDING: block size: {}  constant: {}'.format(detector.thresholding_block_size, detector.thresholding_constant))

            elif key in {ord('e'), ord('d'), ord('r'), ord('f')}:
                if key == ord('e'):
                    detector.morphology_open_kernel_size += 1
                elif key == ord('d'):
                    detector.morphology_open_kernel_size -= 1
                elif key == ord('r'):
                    detector.morphology_close_kernel_size += 1
                elif key == ord('f'):
                    detector.morphology_close_kernel_size -= 1
                print('KERNEL SIZE: open: {}  close: {}'.format(detector.morphology_open_kernel_size, detector.morphology_close_kernel_size))

            elif key in {ord('t'), ord('g')}:
                if key == ord('t'):
                    detector.box_min_area += 2
                elif key == ord('g'):
                    detector.box_min_area -= 2
                print('BOX AREA: {}'.format(detector.box_min_area))

            elif key in {ord('u'), ord('j')}:
                if key == ord('u'):
                    detector.box_min_temp += 0.1
                elif key == ord('j'):
                    detector.box_min_temp -= 0.1
                print('MIN BOX TEMP: {} °C'.format(detector.box_min_temp))

            elif 81 <= key <= 84:
                # LEFT
                if key == 81:
                    detector.adaptive_min_temp -= 0.2
                # RIGHT
                elif key == 83:
                    detector.adaptive_min_temp += 0.2
                # UP
                elif key == 82:
                    detector.adaptive_max_temp += 0.2
                # DOWN
                elif key == 84:
                    detector.adaptive_max_temp -= 0.2
                print('TEMP: min: {} °C  max {} °C'.format(detector.adaptive_min_temp, detector.adaptive_max_temp))

            # SPACE
            elif key == ord(' '):
                break

            # ESC
            elif key == 27:
                should_exit = True
                break

    # print stuff
    print('Parameters: {}'.format(detector))
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main()