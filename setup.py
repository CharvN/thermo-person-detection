import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="ThermoDetection",
    version="1.0",
    author="Michal Charvat",
    author_email="charvin@atlas.cz",
    licence="GNU LESSER GENERAL PUBLIC LICENSE v3",
    description="Python3 helper library for the system for detection and localization of people.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/CharvN/thermo-person-detection",
    packages=['ThermoDetection'],
    python_requires='>3.5.2',
)